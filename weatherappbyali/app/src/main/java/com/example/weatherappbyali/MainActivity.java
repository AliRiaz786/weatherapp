package com.example.weatherappbyali;

import android.graphics.Color;
import android.os.Bundle;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;

import android.view.View;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.example.weatherappbyali.databinding.ActivityMainBinding;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.zip.Inflater;

public class MainActivity extends AppCompatActivity {
EditText  etCountry, etCity;
TextView tvResults;
Button res;
    private final String url="http://api.openweathermap.org/data/2.5/weather";
 private final String appid="aea3e8d8e0897e1b3d07c29ba8e0be56";
    DecimalFormat df =new DecimalFormat("#.##");

    private AppBarConfiguration appBarConfiguration;
    private int binding;
    private Inflater ActivityMainBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        res =findViewById(R.id.btnGet);
        etCountry=findViewById(R.id.etCountry);
        etCity = findViewById(R.id.etCity);
        tvResults=findViewById(R.id.tvResult);

        res.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(MainActivity.this,"Button pressed", Toast.LENGTH_SHORT).show();
                getweatherdetails();

            }
        });


    }


    public void getweatherdetails() {
        String tempurl ="";
        String city =etCity.getText().toString().trim();
        String country =etCountry.getText().toString().trim();
        if(city.equals("")){
            tvResults.setText("city field cannot be empty");
        }
        else
        {
            if(!country.equals(""))
            {
                tempurl =url + "?q=" + city + "," + country + "&appid=" + appid;
            }
            else{
                tempurl =url + "?q=" + city  + "&appid=" + appid;
            }
            StringRequest stringRequest =new StringRequest(Request.Method.POST, tempurl, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    String output="";
                    try {
                        JSONObject jsonResponse =new JSONObject(response);
                        JSONArray jsonArray =jsonResponse.getJSONArray("weather");
                        JSONObject jsonObjectWeather = jsonArray.getJSONObject(0);
                        String description = jsonObjectWeather.getString("description");
                        JSONObject jsonobjectMain = jsonResponse.getJSONObject("main");
                        double temp = jsonobjectMain.getDouble("temperature")-273.15;
                        double percip=jsonobjectMain.getDouble("percip")-273.15;
                        float pressure =jsonobjectMain.getInt("pressure");
                        int humidity =jsonobjectMain.getInt("humidity");
                        JSONObject jsonObjectWind =jsonObjectWeather.getJSONObject("wind");
                        String wind = jsonObjectWind.getString("speed");
                        JSONObject jsonObjectClouds = jsonResponse.getJSONObject("clouds");
                        String clouds =jsonObjectClouds.getString("all");
                        JSONObject jsonObjectSys =jsonResponse.getJSONObject("dir");
                        String countryname =jsonResponse.getString("country");
                        String cityname = jsonResponse.getString("name");
                        tvResults.setTextColor(Color.rgb(68,134,199));
                        output += "current weather of " + cityname + " (" + countryname + " )"
                                + "\n temperature :" + df.format(temp) + " ^C"
                                + "\n percip :" + df.format(percip) + " ^C"
                                + "\n humidity :" + humidity + "%"
                                +"\n Description :" + description
                                + "\n wind speed :" + wind + "m/s "
                                + "\n cloudiness:" +clouds + "%"
                                + "\n pressure " + pressure + " hPa";
                        tvResults.setText(output);

                    } catch (JSONException e){
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(MainActivity.this,error.toString().trim()+ "", Toast.LENGTH_SHORT).show();
                }
            });

            RequestQueue requestQueue= Volley.newRequestQueue(getApplicationContext());
            requestQueue.add(stringRequest);
        }

    }
}